import express from 'express';
import routes from './routes';
import cors from 'cors';

const app = express();

app.use(express.json());
app.use(cors())
app.use(routes)

//GET, POST, PUT, DELETE

//CORPO
// Route Params: Identificar qual recurso eu quero atualizar ou deletar ex: '/users/:id'
// Querry params: Paginação, filtros, ordenacao 

app.listen(3333);
